FROM python:3.8-alpine AS Builder

# Install dependencies
RUN pip install pipenv
# COPY Pipfile* /
RUN pipenv lock --requirements > requeriments.txt
RUN apk add --update --no-cache --virtual .tmp gcc libc-dev
RUN pip install django
RUN pip install django-autocomplete-light
RUN pip install weasyprint
RUN pip install -r requeriments.txt

# Copy sources files
WORKDIR /code
COPY . .

# Default port
ARG ARG_DEFAULT_PORT=8000
EXPOSE $ARG_DEFAULT_PORT
ENV DEFAULT_PORT=${ARG_DEFAULT_PORT}

# Install migrations
RUN python manage.py makemigrations
RUN python manage.py migrate

# Run server
ENTRYPOINT python manage.py runserver 0.0.0.0:${DEFAULT_PORT}