TIPO_DOC = (
    ('DNI', 'D.N.I.'),
    ('LC', 'L.C.'),
    ('LE', 'L.E.'),
    ('PAS', 'PASAPORTE'),
)

ESTADOS_CONTRIB = (
    (0, 'Baja'),
    (1, 'Activo'),
)