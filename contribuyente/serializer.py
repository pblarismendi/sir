from rest_framework import serializers
from .models import Contribuyente


class ContribuyenteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contribuyente
        fields = ('id',
                  'apellido',
                  'nombre',
                  'tipoDoc',
                  'numDoc',
                  'fecNac',
                  'nacionalidad',
                  'estado')

    def create(self, validated_data):
            instance = Contribuyente.objects.create(**validated_data)
            return instance
