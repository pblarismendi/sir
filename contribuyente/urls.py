# Imports de Django
from django.conf.urls import url
from django.urls import path
# Imports de la app
from . import views

# Definimos nuestros Paths
from .views import *

app_name = 'contribuyentes'
urlpatterns = [
    path('', views.menu, name='menu'),
    path('lista_contrib', views.lista_contribuyentes, name='lista_contribuyentes'),
    path('cargar_contrib', views.cargar_contribuyente, name='cargar_contribuyente'),
    path('mod_contrib/<int:contribuyente_id>', views.cargar_contribuyente, name='mod_contribuyente'),
    path('activar/<int:contribuyente_id>', views.activar_contribuyente, name='activar_contribuyente'),
    path('baja/<int:contribuyente_id>', views.baja_contribuyente, name='baja_contribuyente'),

    # Angular
    url(r'^api/contrib$', ContribuyenteAPIview.as_view(), name='contrib'),
    path('api/contrib/<int:numDoc>', views.contrib_detail),
]
