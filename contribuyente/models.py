from django.db import models
from .choices import TIPO_DOC, ESTADOS_CONTRIB
from core.choices import NACIONALIDADES
from django.urls import reverse
from django.utils import timezone

# Create your models here.
class Contribuyente(models.Model):
    apellido = models.CharField("Apellido", max_length=100)
    nombre = models.CharField("Nombres", max_length=200)
    tipoDoc = models.CharField("Tipo Doc", max_length=3, choices=TIPO_DOC, default='DNI')
    numDoc = models.IntegerField("Num Documento", null=False, blank=False, default=0)
    fecNac = models.DateField("Fecha Nacimiento", default=timezone.now)
    nacionalidad = models.IntegerField("nacionalidad", choices=NACIONALIDADES, default=200)
    estado = models.IntegerField("Estado", choices=ESTADOS_CONTRIB, default=1)


    class Meta:
        verbose_name = "Contribuyente"
        verbose_name_plural = "Contribuyentes"


    def __str__(self):
        return self.apellido + ', ' + self.nombre

    def get_absolute_url(self):
        return reverse("Contribuyente_detail", kwargs={"pk": self.pk})
