from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework import generics, mixins
from django.views.decorators.csrf import csrf_exempt
from .serializer import ContribuyenteSerializer
from .models import Contribuyente
from .forms import ContribuyenteForm
from core.choices import NACIONALIDADES
from django.contrib.auth.decorators import permission_required
from .functions import cargarCaracteristicas
from django.db.models import Q


# Create your views here.
@permission_required('usuarios.ejecutivos')
def menu(request):
    return render(request, 'menu_contribuyentes.html', {})


@permission_required('usuarios.ejecutivos')
def lista_contribuyentes(request):
    contribuyentes = Contribuyente.objects.all()
    for i in contribuyentes:
        for j in NACIONALIDADES:
            if i.nacionalidad == j[0]:
                i.nacionalidad = j[1]
    return render(request, 'lista_contribuyentes.html', {
        'contribuyentes': contribuyentes,
        'has_table': True,
    })


@permission_required('usuarios.ejecutivos')
def cargar_contribuyente(request, contribuyente_id=None):
    contribuyente = None
    if contribuyente_id:
        contribuyente = Contribuyente.objects.get(pk=contribuyente_id)
        form = ContribuyenteForm(instance=contribuyente)
        if request.method == 'POST':
            form = ContribuyenteForm(request.POST, instance=contribuyente)
            if form.is_valid():
                form.save()
                return redirect('contribuyentes:lista_contribuyentes')
    else:
        form = ContribuyenteForm()
        if request.method == 'POST':
            form = ContribuyenteForm(request.POST)
            if form.is_valid():
                form.save()
                nuevo = form['numDoc'].value()
                nuevoContrib = Contribuyente.objects.filter(numDoc=nuevo).first()
                valor = str(nuevoContrib.id)
                cargarCaracteristicas(valor)
                return redirect('contribuyentes:lista_contribuyentes')
    return render(request, "extras/generic_form.html",
                  {'titulo': "Cargar Contribuyente", 'form': form, 'boton': "Guardar", })


@permission_required('usuarios.ejecutivos')
def baja_contribuyente(request, contribuyente_id):
    contribuyente = Contribuyente.objects.get(pk=contribuyente_id)
    contribuyente.estado = 0
    contribuyente.save()
    return redirect('contribuyentes:lista_contribuyentes')


@permission_required('usuarios.ejecutivos')
def activar_contribuyente(request, contribuyente_id):
    contribuyente = Contribuyente.objects.get(pk=contribuyente_id)
    contribuyente.estado = 1
    contribuyente.save()
    return redirect('contribuyentes:lista_contribuyentes')


@api_view(['GET', 'POST', 'DELETE'])
def contrib_list(request):
    contribuyente = Contribuyente.objects.all().values()
    return JsonResponse({'contribuyentes': list(contribuyente)})


@api_view(['GET', 'PUT', 'DELETE'])
def contrib_detail(request, numDoc):
    try:
        if request.method == 'GET':
            contribuyente = Contribuyente.objects.filter(numDoc=numDoc).values()
            return JsonResponse({'contribuyentes': list(contribuyente)})
    except Contribuyente.DoesNotExist:
        return JsonResponse({'message': 'El contribuyente no existe'}, status=status.HTTP_404_NOT_FOUND)


class ContribuyenteAPIview(mixins.CreateModelMixin, generics.ListAPIView):
    lookup_field = "pk"
    serializer_class = ContribuyenteSerializer

    def get_queryset(self):
        qs = Contribuyente.objects.all()
        query = self.request.GET.get("q")
        if query is not None:
            qs = qs.filter(
                Q(numDoc=query)
            ).distinct()
        return qs

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
