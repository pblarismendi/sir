from .models import Contribuyente
from bromatologia.forms import HabilNegociosForm
from django.shortcuts import HttpResponse
import sqlite3


def cargarCaracteristicas(nuevo):
    conn = sqlite3.connect('db.sqlite3')
    cursor = conn.cursor()
    cursor.execute("INSERT INTO bromatologia_habilnegocios('contribuyente_id', \
                    'nota', 'fotoDNI', 'fotoCarnet', 'fotEscritura', 'croquis', \
                    'libreDeuda', 'inspeccionComercio', 'inspeccionObras', 'matafuego')"
                    +"VALUES(" + nuevo + ",0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0)")
    cursor.execute("INSERT INTO bromatologia_vendedoresambulantes('contribuyente_id', \
                    'nota', 'carnet', 'carpa', 'permiso', 'autorizacion', 'certBPM', \
                    'agua', 'cofia')"
                    +"VALUES(" + nuevo + ",0 ,0 ,0 ,0 ,0 ,0 ,0 ,0)")
    cursor.execute("INSERT INTO bromatologia_habilremis('contribuyente_id', \
                    'nota', 'fotoDNI', 'FotoSani', 'fotoCarnet', 'planilla', 'titulo', \
                    'contrato', 'cedula', 'seguro', 'inspeccion', 'itn', 'libre')"
                    +"VALUES(" + nuevo + ",0 ,0 ,0 ,0 ,0 ,0 ,0 ,0, 0, 0, 0, 0)")
    conn.commit()
    conn.close()
