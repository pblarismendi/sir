# Generated by Django 2.2.5 on 2020-11-20 19:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contribuyente', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contribuyente',
            name='numDoc',
            field=models.IntegerField(default=0, verbose_name='Num Documento'),
        ),
    ]
