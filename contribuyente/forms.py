# Imports Python
from datetime import date
# Imports Django
from django.utils import timezone
from django import forms
from django.contrib.auth.models import User
from django.forms.widgets import CheckboxSelectMultiple
# Imports extra

# Imports del proyecto
from core.widgets import XDSoftDatePickerInput
# Imports de la app
from .models import Contribuyente


class ContribuyenteForm(forms.ModelForm):    
    class Meta:
        model = Contribuyente
        fields = '__all__'
        widgets = {
            'fecNac': XDSoftDatePickerInput(attrs={'autocomplete':'on'}),
        }

