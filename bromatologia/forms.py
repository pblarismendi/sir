# Imports Django
from django import forms

# Imports de la app
from .models import HabilNegocios, VendedoresAmbulantes, HabilRemis, Carnet


class selectContribForm(forms.ModelForm):
    class Meta:
        model = HabilNegocios
        fields = ['contribuyente']


class HabilNegociosForm(forms.ModelForm):
    class Meta:
        model = HabilNegocios
        fields = '__all__'
        exclude = ('contribuyente',)
        

class VendedoresAmbulantesForm(forms.ModelForm):
    class Meta:
        model = VendedoresAmbulantes
        fields = '__all__'
        exclude = ('contribuyente',)


class HabilRemisForm(forms.ModelForm):
    class Meta:
        model = HabilRemis
        fields = '__all__'
        exclude = ('contribuyente',)


class CarnetForm(forms.ModelForm):
    class Meta:
        model = Carnet
        fields = '__all__'
        