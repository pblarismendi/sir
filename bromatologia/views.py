#imports de django
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import HttpResponse
from django.contrib.auth.decorators import permission_required
from django.template.loader import render_to_string
from weasyprint import HTML
from weasyprint.fonts import FontConfiguration

#imports del proyecto
from .forms import HabilNegociosForm
from .forms import selectContribForm
from .forms import VendedoresAmbulantesForm
from .forms import HabilRemisForm
from .forms import CarnetForm
from contribuyente.models import Contribuyente
from .models import HabilNegocios
from .models import VendedoresAmbulantes
from .models import HabilRemis
from .models import Carnet

# Create your views here.

@permission_required('usuarios.ejecutivos')
def menu(request):
    return render(request, 'menu_bromatologia.html', {})


# Habilitación de negocios

@permission_required('usuarios.ejecutivos')
def selectContrib(request):
    form = selectContribForm()
    if request.method == 'POST':
        form = request.POST['contribuyente']
        return redirect('bromatologia:contribNegocios', contribuyente_id=form)
    return render(request, "extras/generic_form.html", {
        'titulo': "Habilitación de Negocios", 
        'form': form, 
        'boton': "Siguiente --->>", })
    

@permission_required('usuarios.ejecutivos')
def habilNegocios(request, contribuyente_id):
    contribuyente = Contribuyente.objects.get(pk=contribuyente_id)
    contribHabil = HabilNegocios.objects.get(contribuyente_id=contribuyente.id)
    form = HabilNegociosForm(instance=contribHabil)
    if form.is_valid():
        form.save()
    contribuyente2 = HabilNegocios.objects.get(contribuyente=contribuyente.id)
    form = HabilNegociosForm(instance=contribuyente2)
    if request.method == 'POST':
        form = HabilNegociosForm(request.POST, instance=contribuyente2)
        if form.is_valid():
            form.save()
            return redirect('bromatologia:contribNegocios', contribuyente_id=contribuyente.id)
    return render(request, "extras/generic_form.html", {
        'titulo': "Habilitación de Negocios", 
        'form': form, 
        'boton': "Guardar", })


@permission_required('usuarios.ejecutivos')
def contribNegocios(request, contribuyente_id):
    contribuyente = HabilNegocios.objects.get(contribuyente_id=contribuyente_id)
    return render(request, 'negocios/contribNegocios.html', {
        'contrib': contribuyente,
        })


# Vendedores Ambulantes

@permission_required('usuarios.ejecutivos')
def selectContrib2(request):
    form = selectContribForm()
    if request.method == 'POST':
        form = request.POST['contribuyente']
        return redirect('bromatologia:contribVendedor', contribuyente_id=form)
    return render(request, "extras/generic_form.html", {
        'titulo': "Vendedores Ambulantes", 
        'form': form, 
        'boton': "Siguiente --->>", })
    

@permission_required('usuarios.ejecutivos')
def habilVendedor(request, contribuyente_id):
    contribuyente = Contribuyente.objects.get(pk=contribuyente_id)
    contribVende = VendedoresAmbulantes.objects.get(contribuyente_id=contribuyente.id)
    form = HabilNegociosForm(instance=contribVende)
    if form.is_valid():
        form.save()
    contribuyente2 = VendedoresAmbulantes.objects.get(contribuyente=contribuyente.id)
    form = VendedoresAmbulantesForm(instance=contribuyente2)
    if request.method == 'POST':
        form = VendedoresAmbulantesForm(request.POST, instance=contribuyente2)
        if form.is_valid():
            form.save()
            return redirect('bromatologia:contribVendedor', contribuyente_id=contribuyente.id)
    return render(request, "extras/generic_form.html", {
        'titulo': "Vendedores Ambulantes", 
        'form': form, 
        'boton': "Guardar", })


@permission_required('usuarios.ejecutivos')
def contribVendedor(request, contribuyente_id):
    contribuyente = VendedoresAmbulantes.objects.get(contribuyente_id=contribuyente_id)
    return render(request, 'negocios/contribVendedor.html', {
        'contrib': contribuyente,
        })


# Habilitación de Remises

@permission_required('usuarios.ejecutivos')
def selectContrib3(request):
    form = selectContribForm()
    if request.method == 'POST':
        form = request.POST['contribuyente']
        return redirect('bromatologia:contribRemis', contribuyente_id=form)
    return render(request, "extras/generic_form.html", {
        'titulo': "Habilitacion para Remises", 
        'form': form, 
        'boton': "Siguiente --->>", })
    

@permission_required('usuarios.ejecutivos')
def habilRemis(request, contribuyente_id):
    contribuyente = Contribuyente.objects.get(pk=contribuyente_id)
    contribRemis = HabilRemis.objects.get(contribuyente_id=contribuyente.id)
    form = HabilRemisForm(instance=contribRemis)
    if form.is_valid():
        form.save()
    contribuyente2 = HabilRemis.objects.get(contribuyente=contribuyente.id)
    form = HabilRemisForm(instance=contribuyente2)
    if request.method == 'POST':
        form = HabilRemisForm(request.POST, instance=contribuyente2)
        if form.is_valid():
            form.save()
            return redirect('bromatologia:contribRemis', contribuyente_id=contribuyente.id)
    return render(request, "extras/generic_form.html", {
        'titulo': "Habilitación para remises", 
        'form': form, 
        'boton': "Guardar", })


@permission_required('usuarios.ejecutivos')
def contribRemis(request, contribuyente_id):
    contribuyente = HabilRemis.objects.get(contribuyente_id=contribuyente_id)
    return render(request, 'negocios/contribRemis.html', {
        'contrib': contribuyente,
        })

#TODO Corregir la generación del PDF para el carnet
#Carnet Sanitario
def carnet(request):
    form = CarnetForm()
    if request.method == 'POST':
        form = CarnetForm(request.POST)
        return redirect('bromatologia:export-pdf', form)
    return render(request, "extras/generic_form.html", {
        'titulo': "Carnet de Sanidad", 
        'form': form, 
        'boton': "Generar", })

def export_pdf(request, data):  
    #context = {}
    html = render_to_string("negocios/carnet.html", data)
    response = HttpResponse(content_type="application/pdf")
    response["Content-Disposition"] = "inline; report.pdf"
    font_config = FontConfiguration()
    HTML(string=html).write_pdf(response, font_config=font_config)
    return response