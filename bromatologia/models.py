from django.db import models
from contribuyente.models import Contribuyente

# Create your models here.
class HabilNegocios(models.Model):
    contribuyente = models.ForeignKey(Contribuyente, on_delete=models.CASCADE)
    nota = models.BooleanField('Nota', default=False)
    fotoDNI = models.BooleanField('Fotocopia DNI', default=False)
    fotoCarnet = models.BooleanField('Fotocopia Carnet Sanitario', default=False)
    fotEscritura = models.BooleanField('Fotocopia Título/Contrato de Propiedad', default=False)
    croquis = models.BooleanField('Croquis del Local', default=False)
    libreDeuda = models.BooleanField('Certificado Libre de Deuda', default=False)
    inspeccionComercio = models.BooleanField('Inspección de Comercio', default=False)
    inspeccionObras = models.BooleanField('Inspección de Obras Públicas', default=False)
    matafuego = models.BooleanField('Matafuego', default=False)


class VendedoresAmbulantes(models.Model):
    contribuyente = models.ForeignKey(Contribuyente, on_delete=models.CASCADE)
    nota = models.BooleanField('Nota', default=False)
    carnet = models.BooleanField('Carnet Sanitario', default=False)
    carpa = models.BooleanField('Carpa o Gazebo en buenas condiciones', default=False)
    permiso = models.BooleanField('Permiso del propietario de la vereda', default=False)
    autorizacion = models.BooleanField('Autorización Municipal', default=False)
    certBPM = models.BooleanField('Certificado BPM', default=False)
    agua = models.BooleanField('Poseer Agua Potable', default=False)
    cofia = models.BooleanField('Uniforme y Cofia', default=False)
    

class HabilRemis(models.Model):
    contribuyente = models.ForeignKey(Contribuyente, on_delete=models.CASCADE)
    nota = models.BooleanField('Nota', default=False)
    fotoDNI = models.BooleanField('Fotocopia D.N.I.', default=False)
    fotoSani = models.BooleanField('Fotocopia Carnet Sanitario', default=False)
    fotoCarnet = models.BooleanField('Fotocopia Carnet de Conducir', default=False)
    planilla = models.BooleanField('Planilla Prontuarial', default=False)
    titulo = models.BooleanField('Título del Automotor', default=False)
    contrato = models.BooleanField('Contrato de locación del vehículo o autorización', default=False)
    cedula = models.BooleanField('Cédula Verde', default=False)
    seguro = models.BooleanField('Seguro del Vehículo - Patente', default=False)
    inspeccion = models.BooleanField('Inspección y Desinfección del vehículo', default=False)
    itn = models.BooleanField('Inspección Técnica Nacional', default=False)
    libre = models.BooleanField('Libre de Deuda Municipal', default=False)


class Carnet(models.Model):
    nombre = models.CharField('Nombre', max_length=200)
    dni = models.CharField('L.E. - L.C. - D.N.I.', max_length=200)
    domicilio = models.CharField('Domicilio', max_length=200)
    edad = models.CharField('Edad', max_length=200)
    nacionalidad = models.CharField('Nacionalidad', max_length=200)
    domlaboral = models.CharField('Domicio donde Trabaja', max_length=200)
    presenta = models.CharField('Para ser Presentado', max_length=200)
    estado = models.CharField('Estado', max_length=200)
    profesion = models.CharField('Profesión', max_length=200)
    sangre = models.CharField('Grupo Sanguíneo', max_length=200)
