# Generated by Django 2.2.5 on 2021-01-08 21:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contribuyente', '0003_auto_20201120_2050'),
        ('bromatologia', '0004_vendedoresambulantes'),
    ]

    operations = [
        migrations.CreateModel(
            name='HabilRemis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nota', models.BooleanField(default=False, verbose_name='Nota')),
                ('fotDNI', models.BooleanField(default=False, verbose_name='Fotocopia D.N.I.')),
                ('fotoSani', models.BooleanField(default=False, verbose_name='Fotocopia Carnet Sanitario')),
                ('fotoCarnet', models.BooleanField(default=False, verbose_name='Fotocopia Carnet de Conducir')),
                ('planilla', models.BooleanField(default=False, verbose_name='Planilla Prontuarial')),
                ('titulo', models.BooleanField(default=False, verbose_name='Título del Automotor')),
                ('contrato', models.BooleanField(default=False, verbose_name='Contrato de locación del vehículo o autorización')),
                ('cedula', models.BooleanField(default=False, verbose_name='Cédula Verde')),
                ('seguro', models.BooleanField(default=False, verbose_name='Seguro del Vehículo - Patente')),
                ('inspeccion', models.BooleanField(default=False, verbose_name='Inspección y Desinfección del vehículo')),
                ('itn', models.BooleanField(default=False, verbose_name='Inspección Técnica Nacional')),
                ('libre', models.BooleanField(default=False, verbose_name='Libre de Deuda Municipal')),
                ('contribuyente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contribuyente.Contribuyente')),
            ],
        ),
    ]
