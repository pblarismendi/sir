from django.apps import AppConfig


class BromatologiaConfig(AppConfig):
    name = 'bromatologia'
