#Imports de Django
from django.conf.urls import url
from django.urls import path
#Imports de la app
from . import views
#Definimos nuestros Paths

app_name = 'bromatologia'

urlpatterns = [
    path('', views.menu, name='menu'),
    #Habilitacion de negocios
    path('selectContrib', views.selectContrib, name='selectContrib'),
    path('contribNegocios/<int:contribuyente_id>', views.contribNegocios, name='contribNegocios'),
    path('habilitacion_negocios/<int:contribuyente_id>', views.habilNegocios, name='habilNegocios'),
    #Vendedores Ambulantes
    path('selectContrib2', views.selectContrib2, name='selectContrib2'),
    path('contribVendedor/<int:contribuyente_id>', views.contribVendedor, name='contribVendedor'),
    path('habilitacion_vendedor/<int:contribuyente_id>', views.habilVendedor, name='habilVendedor'),
    #Habilitacion remises
    path('selectContrib3', views.selectContrib3, name='selectContrib3'),
    path('contribRemis/<int:contribuyente_id>', views.contribRemis, name='contribRemis'),
    path('habilitacion_remis/<int:contribuyente_id>', views.habilRemis, name='habilRemis'),
    #carnet sanitario
    path('carnet', views.carnet, name='carnet'),
    path('carnet/export/<data>', views.export_pdf, name="export-pdf" ),
]