from django.apps import AppConfig


class OpublicaConfig(AppConfig):
    name = 'opublica'
