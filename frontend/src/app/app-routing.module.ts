import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddContribComponent } from './modules/contribuyentes/components/add-contrib/add-contrib.component';
import { ContribListComponent } from './modules/contribuyentes/components/contrib-list/contrib-list.component';
import { ContribDetailsComponent } from './modules/contribuyentes/components/contrib-details/contrib-details.component';
import { HomeComponent } from "./components/home/home.component";
import { RentasComponent } from "./components/rentas/rentas.component";


const routes: Routes = [
  // Home
  { path: '', pathMatch: 'full', component: HomeComponent },

  // Rentas
  { path: 'rentas', pathMatch: 'full', component: RentasComponent },

  // Contribuyentes
  { path: 'contrib', component: ContribListComponent },
  { path: 'contrib/:numDoc', component: ContribDetailsComponent },
  { path: 'contrib_add', component: AddContribComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
