import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const baseUrl = 'https://apis.datos.gob.ar/georef/api';

@Injectable({
  providedIn: 'root'
})
export class GeorefService {

  constructor(private http: HttpClient) { }

  getProvincias(){
    return this.http.get(`${baseUrl}/provincias`);
  }

  getDepartamentos(provincia: string){
    return this.http.get(`${baseUrl}/departamentos?provincia=${provincia}`);
  }

  getLocalidades(departamento: string){
    return this.http.get(`${baseUrl}/localidades?departamento=${departamento}`);
  }

}
