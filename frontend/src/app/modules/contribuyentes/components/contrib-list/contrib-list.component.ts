import { Component, OnInit } from '@angular/core';
import { ContribuyentesService } from '../../contribuyentes.service';
import {FormControl, FormGroup} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-contrib-list',
  templateUrl: './contrib-list.component.html',
  styleUrls: ['./contrib-list.component.scss']
})
export class ContribListComponent implements OnInit {

  contrib: any;
  currentContrib: any;
  currentIndex = -1;
  numDoc: number = 0;
  buscar = false;
  loading = false;
  busqueda = new FormGroup({
    search: new FormControl('')
  })


  constructor(private contribuyentesService: ContribuyentesService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  /*Tabla*/
  dataSourceContrib = new MatTableDataSource<any>();
  displayedColumns = ['contrib'];

  retrieveContrib() {
    this.loading = true;
    this.contribuyentesService.getAll()
      .subscribe((contrib: any) => {
        this.dataSourceContrib = contrib['contribuyentes'];
        console.log(contrib['contribuyentes']);
        this.loading = false;
      },
      error => {
        console.log(error);
        this.loading = false;
      });
  }

  refreshList() {
    this.retrieveContrib();
    this.currentContrib = null;
    this.currentIndex = -1;
  }

  setActiveContrib(contrib: any, index: any) {
    console.log(contrib);
    this.currentContrib = contrib;
    this.currentIndex = index;
  }

//   removeAllTutorials() {
//     this.tutorialService.deleteAll()
//       .subscribe(
//         response => {
//           console.log(response);
//           this.retrieveTutorials();
//         },
//         error => {
//           console.log(error);
//         });
//   }
//

  searchContrib() {
    this.loading = true;
    this.contribuyentesService.findByNombre(this.busqueda.value['search'])
      .subscribe(
        (data: any) => {
          this.dataSourceContrib = data['contribuyentes'];
          if (data['contribuyentes'].length === 0){
            this.snackBar.open('No se encontraron resultados', 'Cerrar', {
              duration: 5000,
              panelClass: ['red-snackbar']
            });
          }
        },
        error => {
          console.log(error);
        });
    this.loading = false;
  }

  borradoform() {
    this.busqueda.reset();
    this.contrib = '';
    this.currentContrib = '';
    this.currentIndex = -1;
    this.numDoc = 0;
    this.buscar = false;
    this.dataSourceContrib = new MatTableDataSource<any>();
  }

  onChange() {
    this.buscar = false;
    if (this.busqueda.value['search'] != ''){
      this.buscar = true;
    }
  }
}
