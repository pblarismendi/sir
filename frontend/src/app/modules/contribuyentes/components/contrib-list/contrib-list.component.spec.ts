import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContribListComponent } from './contrib-list.component';

describe('ContribListComponent', () => {
  let component: ContribListComponent;
  let fixture: ComponentFixture<ContribListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContribListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContribListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
