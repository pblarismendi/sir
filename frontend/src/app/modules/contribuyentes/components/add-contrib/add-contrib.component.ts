import { Component, OnInit } from '@angular/core';
import { ContribuyentesService } from "../../contribuyentes.service";
import { FormControl, FormGroup } from "@angular/forms";
import { DateAdapter } from "@angular/material/core";
import {GeorefService} from "../../../../services/georef.service";
import {Observable} from "rxjs";


interface Option {
  value: string;
  viewValue: string;
}
interface Estado {
  value: number;
  viewValue: string;
}

interface Nacionalidad {
  value: number;
  viewValue: string;
}

interface Provincia {
  value: string;
  viewValue: string;
}

interface Departamento {
  value: string;
  viewValue: string;
}

interface Localidad {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-contrib',
  templateUrl: './add-contrib.component.html',
  styleUrls: ['./add-contrib.component.scss']
})
export class AddContribComponent implements OnInit {

  formContrib = new FormGroup({
    apellido: new FormControl(''),
    nombre: new FormControl(''),
    tipoDoc: new FormControl(''),
    numDoc: new FormControl(''),
    fecNac: new FormControl(''),
    nacionalidad: new FormControl(''),
    estado: new FormControl(''),
    provincia: new FormControl(''),
    departamento: new FormControl(''),
    localidad: new FormControl(''),
    direccion: new FormControl(''),
    email: new FormControl(''),
    telefono: new FormControl(''),
    movil: new FormControl('')
  });
  submitted = false;
  save = false;

  Options: Option[] = [
    {value: 'DNI', viewValue: 'D.N.I.'},
    {value: 'L.E.', viewValue: 'L.E.'},
    {value: 'L.C.', viewValue: 'L.C.'},
    {value: 'PASAPORTE', viewValue: 'PASAPORTE'}
  ];

  Nacionalidades: Nacionalidad[] = [
    {value:301, viewValue: 'Afganistán'},
    {value:401, viewValue: 'Albania'},
    {value:438, viewValue: 'Alemania'},
    {value:404, viewValue: 'Andorra'},
    {value:149, viewValue: 'Angola'},
    {value:237, viewValue: 'Antigua y Barbuda'},
    {value:302, viewValue: 'Arabia Saudita'},
    {value:102, viewValue: 'Argelia'},
    {value:200, viewValue: 'Argentina'},
    {value:349, viewValue: 'Armenia'},
    {value:501, viewValue: 'Australia'},
    {value:405, viewValue: 'Austria'},
    {value:350, viewValue: 'Azerbaiyan'},
    {value:239, viewValue: 'Bahamas'},
    {value:303, viewValue: 'Bahreih'},
    {value:345, viewValue: 'Bangladesh'},
    {value:201, viewValue: 'Barbados'},
    {value:406, viewValue: 'Bélgica'},
    {value:236, viewValue: 'Belice'},
    {value:112, viewValue: 'Benin'},
    {value:439, viewValue: 'Bielorrusia'},
    {value:202, viewValue: 'Bolivia'},
    {value:103, viewValue: 'Botswana'},
    {value:446, viewValue: 'Boznia Herzegovina'},
    {value:203, viewValue: 'Brasil'},
    {value:346, viewValue: 'Brunei'},
    {value:407, viewValue: 'Bulgaria'},
    {value:101, viewValue: 'Burkina Faso'},
    {value:104, viewValue: 'Burundi'},
    {value:305, viewValue: 'Butan'},
    {value:150, viewValue: 'Cabo Verde'},
    {value:306, viewValue: 'Cambodia'},
    {value:105, viewValue: 'Camerún'},
    {value:204, viewValue: 'Canadá'},
    {value:111, viewValue: 'Chad'},
    {value:408, viewValue: 'Checoslovaquia {value:códigohistórico}'},
    {value:208, viewValue: 'Chile'},
    {value:310, viewValue: 'China'},
    {value:435, viewValue: 'Chipre'},
    {value:205, viewValue: 'Colombia'},
    {value:155, viewValue: 'Comoras'},
    {value:108, viewValue: 'Congo'},
    {value:308, viewValue: 'Corea Democratica'},
    {value:309, viewValue: 'Corea Republicana'},
    {value:110, viewValue: 'Costa de Marfil'},
    {value:206, viewValue: 'Costa Rica'},
    {value:447, viewValue: 'Croacia'},
    {value:207, viewValue: 'Cuba'},
    {value:409, viewValue: 'Dinamarca'},
    {value:153, viewValue: 'Djibouti'},
    {value:233, viewValue: 'Dominicana'},
    {value:210, viewValue: 'Ecuador'},
    {value:212, viewValue: 'EEUU'},
    {value:113, viewValue: 'Egipto'},
    {value:211, viewValue: 'El Salvador'},
    {value:331, viewValue: 'Emiratos Arabes'},
    {value:160, viewValue: 'Eritrea'},
    {value:448, viewValue: 'Eslovaquia'},
    {value:449, viewValue: 'Eslovenia'},
    {value:410, viewValue: 'España'},
    {value:440, viewValue: 'Estonia'},
    {value:161, viewValue: 'Etiopia'},
    {value:512, viewValue: 'Fiji'},
    {value:312, viewValue: 'Filipinas'},
    {value:411, viewValue: 'Finlandia'},
    {value:412, viewValue: 'Francia'},
    {value:115, viewValue: 'Gabon'},
    {value:116, viewValue: 'Gambia'},
    {value:351, viewValue: 'Georgia'},
    {value:117, viewValue: 'Ghana'},
    {value:240, viewValue: 'Granada'},
    {value:413, viewValue: 'Grecia'},
    {value:213, viewValue: 'Guatemala'},
    {value:118, viewValue: 'Guinea'},
    {value:156, viewValue: 'Guinea Bissau'},
    {value:119, viewValue: 'Guinea Ecuatorial'},
    {value:214, viewValue: 'Guyana'},
    {value:215, viewValue: 'Haiti'},
    {value:216, viewValue: 'Honduras'},
    {value:414, viewValue: 'Hungria'},
    {value:315, viewValue: 'India'},
    {value:316, viewValue: 'Indonesia'},
    {value:317, viewValue: 'Irak'},
    {value:318, viewValue: 'Iran'},
    {value:415, viewValue: 'Irlanda'},
    {value:416, viewValue: 'Islandia'},
    {value:521, viewValue: 'Islas Marianas'},
    {value:520, viewValue: 'Islas Marshal'},
    {value:518, viewValue: 'Islas Salomon'},
    {value:319, viewValue: 'Israel'},
    {value:417, viewValue: 'Italia'},
    {value:217, viewValue: 'Jamaica'},
    {value:320, viewValue: 'Japón'},
    {value:321, viewValue: 'Jordania'},
    {value:352, viewValue: 'Kazajstan'},
    {value:120, viewValue: 'Kenya'},
    {value:353, viewValue: 'Kirguistan'},
    {value:514, viewValue: 'Kiribati'},
    {value:323, viewValue: 'Kuwait'},
    {value:324, viewValue: 'Laos'},
    {value:121, viewValue: 'Lesotho'},
    {value:441, viewValue: 'Letonia'},
    {value:325, viewValue: 'Libano'},
    {value:122, viewValue: 'Liberia'},
    {value:123, viewValue: 'Libia'},
    {value:418, viewValue: 'Liechtenstein'},
    {value:442, viewValue: 'Lituania'},
    {value:419, viewValue: 'Luxemburgo'},
    {value:450, viewValue: 'Macedonia'},
    {value:124, viewValue: 'Madagascar'},
    {value:326, viewValue: 'Malasia'},
    {value:125, viewValue: 'Malawi'},
    {value:327, viewValue: 'Maldivas'},
    {value:126, viewValue: 'Mali'},
    {value:420, viewValue: 'Malta'},
    {value:127, viewValue: 'Marruecos'},
    {value:128, viewValue: 'Mauricio'},
    {value:129, viewValue: 'Mauritania'},
    {value:218, viewValue: 'Mexico'},
    {value:515, viewValue: 'Micronesia Estados Federados'},
    {value:443, viewValue: 'Moldova'},
    {value:421, viewValue: 'Mónaco'},
    {value:329, viewValue: 'Mongolia'},
    {value:151, viewValue: 'Mozambique'},
    {value:158, viewValue: 'Namibia'},
    {value:503, viewValue: 'Nauru'},
    {value:330, viewValue: 'Nepal'},
    {value:219, viewValue: 'Nicaragua'},
    {value:130, viewValue: 'Niger'},
    {value:131, viewValue: 'Nigeria'},
    {value:422, viewValue: 'Noruega'},
    {value:504, viewValue: 'Nueva Zelanda'},
    {value:328, viewValue: 'Oman'},
    {value:423, viewValue: 'Paises Bajos'},
    {value:332, viewValue: 'Pakistan'},
    {value:516, viewValue: 'Palau'},
    {value:220, viewValue: 'Panama'},
    {value:513, viewValue: 'Papua Nueva Guinea'},
    {value:221, viewValue: 'Paraguay'},
    {value:222, viewValue: 'Perú'},
    {value:424, viewValue: 'Polonia'},
    {value:425, viewValue: 'Portugal'},
    {value:223, viewValue: 'Puerto Rico'},
    {value:322, viewValue: 'Qatar'},
    {value:426, viewValue: 'R.Unido'},
    {value:107, viewValue: 'Rep. Centro Africana'},
    {value:209, viewValue: 'Rep. Dominicana'},
    {value:451, viewValue: 'Republica Checa'},
    {value:348, viewValue: 'Republica de Yemén'},
    {value:427, viewValue: 'Rumania'},
    {value:444, viewValue: 'Rusia'},
    {value:133, viewValue: 'Rwanda'},
    {value:235, viewValue: 'S. Vicente Granadas'},
    {value:157, viewValue: 'S.Tomey Principe'},
    {value:506, viewValue: 'Samoa Occidental'},
    {value:238, viewValue: 'San Cristobal y Nevis'},
    {value:428, viewValue: 'San Marino'},
    {value:234, viewValue: 'Santa Lucia'},
    {value:431, viewValue: 'Santa Sede (Vaticano)'},
    {value:134, viewValue: 'Senegal'},
    {value:152, viewValue: 'Seychelles'},
    {value:135, viewValue: 'Sierra Leona'},
    {value:333, viewValue: 'Singapur'},
    {value:334, viewValue: 'Siria'},
    {value:136, viewValue: 'Somalia'},
    {value:307, viewValue: 'Sri Lanca'},
    {value:159, viewValue: 'Sudafrica'},
    {value:138, viewValue: 'Sudán'},
    {value:429, viewValue: 'Suecia'},
    {value:430, viewValue: 'Suiza'},
    {value:232, viewValue: 'Suriname'},
    {value:137, viewValue: 'Swazilandia'},
    {value:335, viewValue: 'Tailandia'},
    {value:313, viewValue: 'Taiwan'},
    {value:139, viewValue: 'Tanzania'},
    {value:354, viewValue: 'Tayikistan'},
    {value:357, viewValue: 'Territorios Autonomos Palestinos'},
    {value:140, viewValue: 'Togo'},
    {value:224, viewValue: 'Trinidad y Tobago'},
    {value:141, viewValue: 'Túnez'},
    {value:355, viewValue: 'Turkmenistan'},
    {value:436, viewValue: 'Turquia'},
    {value:445, viewValue: 'Ucrania'},
    {value:142, viewValue: 'Uganda'},
    {value:304, viewValue: 'Union de Myanmar'},
    {value:225, viewValue: 'Uruguay'},
    {value:356, viewValue: 'Uzbekistan'},
    {value:226, viewValue: 'Venezuela'},
    {value:337, viewValue: 'Vietnam'},
    {value:452, viewValue: 'Yugoeslavia'},
    {value:109, viewValue: 'Zaire'},
    {value:144, viewValue: 'Zambia'},
    {value:132, viewValue: 'Zimbabwe'},
  ]

  Estados: Estado[] = [
    {value: 1, viewValue: "Activo"},
    {value: 2, viewValue: "Baja"}
  ];

  Provincias: any;

  Departamentos: any;

  Localidades: any;

  constructor(private contribuyentesService: ContribuyentesService,
              private georef: GeorefService,
              private dateAdapter: DateAdapter<Date>) { }

  ngOnInit(): void {
    this.georef.getProvincias().subscribe((res: any) => {
      this.Provincias = res['provincias'];
      }
    );

  }

  formatDate(date: any) {
    let year = date['_i']['year'];
    let month = date['_i']['month'] + 1;
    let day = date['_i']['date'];
    return year + "-" + month + "-" + day;
  }

  saveContrib() {
    const data = {
      "apellido": this.formContrib.value['apellido'],
      "nombre": this.formContrib.value['nombre'],
      "tipoDoc": this.formContrib.value['tipoDoc'],
      "numDoc": this.formContrib.value['numDoc'],
      "fecNac": this.formatDate(this.formContrib.value['fecNac']),
      "nacionalid": this.formContrib.value['nacionalid'],
      "estado": this.formContrib.value['estado'],
    };
    console.log(this.formatDate(this.formContrib.value['fecNac']));

    this.contribuyentesService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });

  }

  onChange() {
    if(this.formContrib.value['apellido'] != '' &&
    this.formContrib.value['nombre'] != '' &&
    this.formContrib.value['tipoDoc'] != '' &&
    this.formContrib.value['numDoc'] != '' &&
    this.formContrib.value['fecNac'] != '' &&
    this.formContrib.value['nacionalidad'] != '' &&
    this.formContrib.value['estado'] != '') {
      this.save = true;
    }
  }


  borradoformContrib() {
    this.formContrib.reset();
    this.save = false;
  }

  onChangeProv() {
    console.log(this.formContrib.value['provincia']);
    this.georef.getDepartamentos(this.formContrib.value['provincia']).subscribe((res: any) => {
      this.Departamentos = res['departamentos'];
    });
  }

  onChangeDep() {
    console.log(this.formContrib.value['departamento']);
    this.georef.getLocalidades(this.formContrib.value['departamento']).subscribe((res: any) => {
      this.Localidades = res['localidades'];
    });
  }
}
