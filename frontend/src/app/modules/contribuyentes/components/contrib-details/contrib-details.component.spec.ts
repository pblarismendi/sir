import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContribDetailsComponent } from './contrib-details.component';

describe('ContribDetailsComponent', () => {
  let component: ContribDetailsComponent;
  let fixture: ComponentFixture<ContribDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContribDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContribDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
