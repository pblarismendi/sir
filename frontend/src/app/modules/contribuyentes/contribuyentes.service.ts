import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const baseUrl = 'http://localhost:8000/contribuyente';

@Injectable({
  providedIn: 'root'
})
export class ContribuyentesService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(`${baseUrl}/api/contrib`);
  }

  get(id: number) {
    return this.http.get(`${baseUrl}/api/contrib/{id}`);
  }

  create(data: any) {
    return this.http.post(`${baseUrl}/api/contrib`, data);
  }

  update(id: number, data: any) {
    return this.http.put(`${baseUrl}/api/contrib/{id}`, data);
  }

  delete(id: number) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

//   deleteAll() {
//     return this.http.delete(baseUrl);
//   }

  findByNombre(numDoc: number) {
    return this.http.get(`${baseUrl}/api/contrib/${numDoc}`);
  }

}
