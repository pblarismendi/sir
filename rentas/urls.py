#Imports de Django
from django.conf.urls import url
from django.urls import path
#Imports de la app
from . import views
#Definimos nuestros Paths

app_name = 'rentas'
urlpatterns = [
    path('', views.menu, name='menu'),
    ]