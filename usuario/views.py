#Imports Django
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.hashers import make_password
from django.contrib.auth.decorators import permission_required

#Import del proyecto

#Imports de la app
from .functions import obtener_permisos, crear_usuario
from .models import Usuario
from .forms import CrearUsuarioForm
from .forms import ModUsuarioForm, ModPassword

# Create your views here.
@permission_required('usuarios.ejecutivo')
def menu(request):
    return render(request, 'menu_usuarios.html', {})

#Manejo de Usuarios
@permission_required('usuarios.ejecutivo')
def listar_operadores(request):
    usuarios = Usuario.objects.all()
    usuarios = usuarios.select_related('usuario')
    return render(request, 'lista_usuario.html', {
        'usuarios': usuarios,
        'has_table': True,
    })

@permission_required('usuarios.ejecutivo')
def crear_operador(request):
    form = CrearUsuarioForm()
    if request.method == "POST":
        form = CrearUsuarioForm(request.POST)
        if form.is_valid():
            operador = form.save(commit=False)
            #Creamos usuario
            if form.cleaned_data['new_user']:#Si marco el check:
                operador.usuario = crear_usuario(operador)
                operador.save()
                return redirect('usuarios:listar_usuarios')
            #Guardamos
            operador.save()
            return redirect('usuarios:listar_usuarios')
    return render(request, "extras/generic_form.html", {'titulo': "Generar Operador", 'form': form, 'boton': "Crear", })

@permission_required('usuarios.ejecutivo')
def mod_operador(request, operador_id=None):
    operador = None
    form = ModUsuarioForm(permisos_list=obtener_permisos(),)
    if operador_id:
        #Si es para modificar conseguimos las bases
        operador = Usuario.objects.get(pk=operador_id)
        usuario = operador.usuario
        permisos_habilitados = [p for p in obtener_permisos(usuario=request.user)]
        id_permisos_habilitados = [p.id for p in obtener_permisos(usuario=usuario)]
        if usuario:
            #Generamos el form
            form = ModUsuarioForm(
                instance=operador,
                permisos_list=permisos_habilitados,
                initial={
                    'username': usuario.username,
                    'permisos': id_permisos_habilitados,
            })
        else:
            form = ModUsuarioForm(
                instance=operador,
                permisos_list=permisos_habilitados,
            )
    if request.method == "POST":
        form = ModUsuarioForm(request.POST, instance=operador)
        if form.is_valid():
            operador = form.save(commit=False)
            #Si nos dio un username
            if form.cleaned_data['username']:
                if not operador.usuario:
                    operador.usuario = crear_usuario(operador)
                    operador.save()
                if operador.usuario.username != form.cleaned_data['username']:
                    operador.usuario.username = form.cleaned_data['username']
                    operador.usuario.save()
            #Reiniciamos sus permisos:
            if usuario:
                for permiso in obtener_permisos():
                    usuario.user_permissions.remove(permiso)
                for permiso in request.POST.getlist('permisos'):
                    usuario.user_permissions.add(permiso)
                usuario.save()
            operador.save()
            return redirect('usuarios:listar_usuarios')
    return render(request, "extras/generic_form.html", {'titulo': "Modificar datos de Usuario", 'form': form, 'boton': "Guardar", })

@permission_required('operadores.operador_admin')
def cambiar_password(request, operador_id):
    operador = Usuario.objects.get(pk=operador_id)
    usuario = operador.usuario
    form = ModPassword(initial={'username': usuario.username, })
    if request.method == 'POST':
        form = ModPassword(request.POST)
        if form.is_valid():
            usuario.password = make_password(form.cleaned_data['passwd1'])
            usuario.save()
            return redirect('usuarios:listar_usuarios')
    #Sea por ingreso o por salida:
    return render(request, "extras/generic_form.html", {'titulo': "Modificar Usuario", 'form': form, 'boton': "Modificar", })

@permission_required('operadores.operador_admin')
def desactivar_usuario(request, operador_id):
    operador = Usuario.objects.get(pk=operador_id)
    usuario = operador.usuario
    usuario.is_active = False
    usuario.save()
    return redirect('usuarios:listar_usuarios')

@permission_required('operadores.operador_admin')
def activar_usuario(request, operador_id):
    operador = Usuario.objects.get(pk=operador_id)
    usuario = operador.usuario
    usuario.is_active = True
    usuario.save()
    return redirect('usuarios:listar_usuarios')