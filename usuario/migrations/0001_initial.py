# Generated by Django 2.2.5 on 2020-11-19 15:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nivel_acceso', models.CharField(choices=[('A', 'Administrador - Solo desarrolladores'), ('E', 'Ejecutivo - Jefes/Responsables de área'), ('O', 'Empleados - Operadores')], default='O', max_length=1, verbose_name='Acceso de Seguridad')),
                ('tipo_doc', models.IntegerField(choices=[(1, 'CI'), (2, 'DNI'), (3, 'LC'), (4, 'LE'), (5, 'Pasaporte')], default=2)),
                ('num_doc', models.CharField(max_length=100, unique=True, verbose_name='Numero de Documento')),
                ('apellidos', models.CharField(max_length=100, verbose_name='Apellidos')),
                ('nombres', models.CharField(max_length=100, verbose_name='Nombres')),
                ('email', models.EmailField(max_length=254, verbose_name='Correo Electronico')),
                ('telefono', models.CharField(default='+549388', max_length=20, verbose_name='Telefono')),
                ('usuario', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='usuario', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Usuarios',
                'ordering': ['apellidos', 'nombres'],
                'permissions': (('admin', 'Administrador del Sistema.'), ('ejecutivo', 'Jefe/Responsable de área'), ('empleado', 'Empleado/Operador')),
            },
        ),
    ]
