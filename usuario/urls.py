# Imports de Django
from django.conf.urls import url
from django.urls import path
# Imports de la app
from . import views
from . import autocomplete

# Definimos nuestros Paths

app_name = 'usuarios'
urlpatterns = [
    path('', views.listar_operadores, name='listar_usuarios'),
    #ABM Usuarios
    path('listar/op', views.listar_operadores, name='listar_usuarios'),
    path('crear/op', views.crear_operador, name='crear_usuario'),
    path('modificar/op/<int:operador_id>', views.mod_operador, name='modificar_usuario'),
    path('chpass/op/<int:operador_id>', views.cambiar_password, name='cambiar_password'),
    path('desactivar/op/<int:operador_id>', views.desactivar_usuario, name='desactivar_usuario'),
    path('activar/op/<int:operador_id>', views.activar_usuario, name='activar_usuario'),
    #Autocomplete
    # url(r'^operadores-autocomplete/$', autocomplete.UsuariosAutocomplete.as_view(), name='usuarios-autocomplete',),
]