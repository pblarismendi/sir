# Imports Python
from datetime import date
# Imports Django
from django.utils import timezone
from django import forms
from django.contrib.auth.models import User
from django.forms.widgets import CheckboxSelectMultiple
# Imports extra

# Imports del proyecto
from core.widgets import XDSoftDatePickerInput
# Imports de la app
from .models import Usuario


# Definimos nuestros forms
class CrearUsuarioForm(forms.ModelForm):
    new_user = forms.BooleanField(label="Crear Usuario", initial=False, required=False)

    class Meta:
        model = Usuario
        fields = '__all__'
        exclude = ('usuario',)


class ModUsuarioForm(forms.ModelForm):
    username = forms.CharField(label='Usuario', max_length=15, min_length=6, required=False)
    permisos = forms.MultipleChoiceField(
        label='Permisos',
        widget=CheckboxSelectMultiple(attrs={'class': 'multiplechoice', }),
        required=False,
    )

    class Meta:
        model = Usuario
        fields = '__all__'
        exclude = ('usuario',)

    # Inicializacion
    def __init__(self, *args, **kwargs):
        # Obtenemos permisos
        permisos_list = kwargs.pop('permisos_list', None)
        if permisos_list:
            self.base_fields['permisos'].choices = [(p.id, p.name) for p in permisos_list]
        super(ModUsuarioForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        if not hasattr(self, 'instance') and User.objects.filter(username=self.cleaned_data['username']):
            raise forms.ValidationError(
                "El usuario indicado ya esta en uso, si el usuario tiene mas de una funcion, contacte al administrador")
        return self.cleaned_data['username']

    def clean_email(self):
        if not hasattr(self, 'instance') and User.objects.filter(email=self.cleaned_data['email']):
            raise forms.ValidationError("El mail indicado ya esta en uso, contacte al administrador")
        return self.cleaned_data['email']


class ModPassword(forms.Form):
    username = forms.CharField(label='Usuario', widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    passwd1 = forms.CharField(label="Password", max_length=32, widget=forms.PasswordInput)
    passwd2 = forms.CharField(label="Repetir Password", max_length=32, widget=forms.PasswordInput)

    def clean_passwd2(self):
        passwd1 = self.cleaned_data['passwd1']
        passwd2 = self.cleaned_data['passwd2']
        if passwd1 != passwd2:
            raise forms.ValidationError("Las contraseñas no son iguales")
        return ''
