#Imports Django
from django.contrib import admin
#Imports extras
#Imports de proyecto
#Imports de la app
from .models import Usuario

#Definimos nuestros modelos administrables:
class UsuarioAdmin(admin.ModelAdmin):
    model = Usuario
    list_filter = ['nivel_acceso']
    search_fields = ['usuario__first_name', 'usuario__last_name', 'num_doc']

# Register your models here.
admin.site.register(Usuario, UsuarioAdmin)