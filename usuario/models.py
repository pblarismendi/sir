from django.db import models
from django.contrib.auth.models import User
#Imports del proyecto:
from core.choices import TIPO_DOCUMENTOS
#improts de la app
from .choices import NIVELES_SEGURIDAD

# Create your models here.
class Usuario(models.Model):
    nivel_acceso = models.CharField("Acceso de Seguridad", max_length=1, choices=NIVELES_SEGURIDAD, default='O')
    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="usuario")
    tipo_doc = models.IntegerField(choices=TIPO_DOCUMENTOS, default=2)
    num_doc = models.CharField('Numero de Documento', unique=True, max_length=100)
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres  = models.CharField('Nombres', max_length=100)
    email = models.EmailField('Correo Electronico')
    telefono = models.CharField('Telefono', max_length=20, default='+549388')
    class Meta:
        verbose_name_plural = 'Usuarios'
        ordering = ['apellidos', 'nombres']
        permissions = (
            #Administador:
            ("admin", "Administrador del Sistema."),
            #Ejecutivo
            ("ejecutivo", "Jefe/Responsable de área"),
            #Empleado
            ("empleado", "Empleado/Operador"),
        )
    def __str__(self):
        return self.apellidos + ', ' + self.nombres