#Imports Django
from django import template
#Imports del proyecto
#Imports de la app
from usuario.functions import obtener_usuario
#Declaramos
register = template.Library()

#Definimos nuestros tags
@register.simple_tag
def ct_get_fecha(operador, dict_eventos):
    try:
        return dict_eventos[usuario.id].fecha.time()
    except KeyError:
        return 'Sin registro'

@register.simple_tag
def ct_get_operador(request):
    return obtener_usuario(request)