#Imports Django
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import permission_required
#Imports del proyecto
from .models import Impuesto
from .forms import ImpuestoForm

# Create your views here.

@permission_required('usuarios.ejecutivos')
def index(request):
    return render(request, 'menu_comercio.html')


@permission_required('usuarios.ejecutivos')
def lista_impuestos(request):
    impuestos = Impuesto.objects.all()
    return render(request, 'impuestos/lista_impuestos.html', {
        'impuestos': impuestos,
        'has_table': True,
    })

@permission_required('usuarios.ejecutivos')
def crear_impuesto(request, impuesto_id=None):
    impuesto = None
    if impuesto_id:
        impuesto = Impuesto.objects.get(pk=impuesto_id)
        form = ImpuestoForm(instance=impuesto)
    else:
        form = ImpuestoForm()
    if request.method == 'POST':
        form = ImpuestoForm(request.POST, instance=impuesto)
        if form.is_valid():
            form.save()
            return redirect('comercio:lista_impuestos')
    return render(request, "extras/generic_form.html", {'titulo': "Nuevo Impuesto", 'form': form, 'boton': "Guardar", })
