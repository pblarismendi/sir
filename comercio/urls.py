#Imports de Django
from django.conf.urls import url
from django.urls import path
#Imports de la app
from . import views
#Definimos nuestros Paths

app_name = 'comercio'
urlpatterns = [
    path('', views.index, name='index'),

    #Impuestos
    path('impuestos/',views.lista_impuestos, name='lista_impuestos'),
    path('impuestos/crear',views.crear_impuesto, name='crear_impuestos'),
    ]