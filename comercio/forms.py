# Imports Django
from django import forms

# Imports de la app
from .models import Impuesto

class ImpuestoForm(forms.ModelForm):
    class Meta:
        model = Impuesto
        fields = '__all__'
