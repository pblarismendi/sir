from django.db import models
from django.utils import timezone
from .choices import ESTADOS

# Create your models here.

class Impuesto(models.Model):
    codImp = models.IntegerField('Código de Impuesto', default=0, null=False, blank=False)
    descImp = models.CharField('Descripción del Impuesto', max_length=200, default='', null=False)
    resImp = models.CharField('Ordenanza/Resolución',max_length=10, default=1, null=False, blank=False)
    fechaImp = models.DateField('Fecha de alta', default=timezone.now)
    bajaImp = models.DateField('Fecha de baja',null=True, blank=True)
    estadoImp = models.SmallIntegerField('Estado', choices=ESTADOS, default=1)
    valorImp = models.DecimalField('Valor', max_digits=10, decimal_places=2, default=0)
